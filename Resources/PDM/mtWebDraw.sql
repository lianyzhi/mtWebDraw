if exists (select 1
            from  sysobjects
           where  id = object_id('tChart')
            and   type = 'U')
   drop table tChart
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tChartDiagram')
            and   name  = 'index_ChartID'
            and   indid > 0
            and   indid < 255)
   drop index tChartDiagram.index_ChartID
go

if exists (select 1
            from  sysobjects
           where  id = object_id('tChartDiagram')
            and   type = 'U')
   drop table tChartDiagram
go

if exists (select 1
            from  sysobjects
           where  id = object_id('tDepartment')
            and   type = 'U')
   drop table tDepartment
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tLogChartDiagram')
            and   name  = 'index_ChartDiagramID'
            and   indid > 0
            and   indid < 255)
   drop index tLogChartDiagram.index_ChartDiagramID
go

if exists (select 1
            from  sysobjects
           where  id = object_id('tLogChartDiagram')
            and   type = 'U')
   drop table tLogChartDiagram
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tUser')
            and   name  = 'index_AuthCode'
            and   indid > 0
            and   indid < 255)
   drop index tUser.index_AuthCode
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tUser')
            and   name  = 'index_DepartmentID'
            and   indid > 0
            and   indid < 255)
   drop index tUser.index_DepartmentID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('tUser')
            and   name  = 'index_UserName'
            and   indid > 0
            and   indid < 255)
   drop index tUser.index_UserName
go

if exists (select 1
            from  sysobjects
           where  id = object_id('tUser')
            and   type = 'U')
   drop table tUser
go

if exists (select 1
            from  sysobjects
           where  id = object_id('tUserChart')
            and   type = 'U')
   drop table tUserChart
go

/*==============================================================*/
/* Table: tChart                                                */
/*==============================================================*/
create table tChart (
   ID                   uniqueidentifier     not null,
   FullName             nvarchar(50)         not null,
   Caption              nvarchar(4000)       null,
   Owner                uniqueidentifier     not null,
   IsDelete             bit                  not null,
   CreateTime           datetime             not null,
   UpdateTime           datetime             not null,
   UpdateUser           uniqueidentifier     not null,
   Remark               nvarchar(400)        null,
   constraint PK_TCHART primary key (ID)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Web图基本信息表',
   'user', @CurrentUser, 'table', 'tChart'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '标识',
   'user', @CurrentUser, 'table', 'tChart', 'column', 'ID'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '名称',
   'user', @CurrentUser, 'table', 'tChart', 'column', 'FullName'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '介绍',
   'user', @CurrentUser, 'table', 'tChart', 'column', 'Caption'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '所有者',
   'user', @CurrentUser, 'table', 'tChart', 'column', 'Owner'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '是否删除',
   'user', @CurrentUser, 'table', 'tChart', 'column', 'IsDelete'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '创建时间',
   'user', @CurrentUser, 'table', 'tChart', 'column', 'CreateTime'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', @CurrentUser, 'table', 'tChart', 'column', 'UpdateTime'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', @CurrentUser, 'table', 'tChart', 'column', 'UpdateUser'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', @CurrentUser, 'table', 'tChart', 'column', 'Remark'
go

/*==============================================================*/
/* Table: tChartDiagram                                         */
/*==============================================================*/
create table tChartDiagram (
   ID                   bigint               identity,
   ChartID              uniqueidentifier     not null,
   DiagramName          nvarchar(50)         not null,
   DiagramContent       ntext                not null,
   IsDelete             bit                  not null,
   CreateTime           datetime             not null,
   UpdateTime           datetime             not null,
   UpdateUser           uniqueidentifier     not null,
   Remark               nvarchar(400)        null,
   constraint PK_TCHARTDIAGRAM primary key (ID)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Web图Diagrams信息表',
   'user', @CurrentUser, 'table', 'tChartDiagram'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '标识',
   'user', @CurrentUser, 'table', 'tChartDiagram', 'column', 'ID'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Web图标识',
   'user', @CurrentUser, 'table', 'tChartDiagram', 'column', 'ChartID'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '图表名称',
   'user', @CurrentUser, 'table', 'tChartDiagram', 'column', 'DiagramName'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '图表内容',
   'user', @CurrentUser, 'table', 'tChartDiagram', 'column', 'DiagramContent'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '是否删除',
   'user', @CurrentUser, 'table', 'tChartDiagram', 'column', 'IsDelete'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '创建时间',
   'user', @CurrentUser, 'table', 'tChartDiagram', 'column', 'CreateTime'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '修改时间',
   'user', @CurrentUser, 'table', 'tChartDiagram', 'column', 'UpdateTime'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', @CurrentUser, 'table', 'tChartDiagram', 'column', 'UpdateUser'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', @CurrentUser, 'table', 'tChartDiagram', 'column', 'Remark'
go

/*==============================================================*/
/* Index: index_ChartID                                         */
/*==============================================================*/
create index index_ChartID on tChartDiagram (
ChartID ASC
)
go

/*==============================================================*/
/* Table: tDepartment                                           */
/*==============================================================*/
create table tDepartment (
   ID                   uniqueidentifier     not null,
   FullName             nvarchar(20)         not null,
   Caption              nvarchar(400)        null,
   constraint PK_TDEPARTMENT primary key (ID)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '用户所属部门表',
   'user', @CurrentUser, 'table', 'tDepartment'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '标识',
   'user', @CurrentUser, 'table', 'tDepartment', 'column', 'ID'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '部门名称',
   'user', @CurrentUser, 'table', 'tDepartment', 'column', 'FullName'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '介绍',
   'user', @CurrentUser, 'table', 'tDepartment', 'column', 'Caption'
go

/*==============================================================*/
/* Table: tLogChartDiagram                                      */
/*==============================================================*/
create table tLogChartDiagram (
   ID                   bigint               identity,
   ChartDiagramID       bigint               not null,
   CreateTime           datetime             not null,
   UserID               uniqueidentifier     not null,
   BakContent           ntext                not null,
   UpdContent           ntext                not null,
   constraint PK_TLOGCHARTDIAGRAM primary key (ID)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '日志表：Web图Diagrams信息表操作日志',
   'user', @CurrentUser, 'table', 'tLogChartDiagram'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '标识',
   'user', @CurrentUser, 'table', 'tLogChartDiagram', 'column', 'ID'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Web图Diagram标识',
   'user', @CurrentUser, 'table', 'tLogChartDiagram', 'column', 'ChartDiagramID'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '操作时间',
   'user', @CurrentUser, 'table', 'tLogChartDiagram', 'column', 'CreateTime'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '操作人',
   'user', @CurrentUser, 'table', 'tLogChartDiagram', 'column', 'UserID'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '备份内容',
   'user', @CurrentUser, 'table', 'tLogChartDiagram', 'column', 'BakContent'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '更改内容',
   'user', @CurrentUser, 'table', 'tLogChartDiagram', 'column', 'UpdContent'
go

/*==============================================================*/
/* Index: index_ChartDiagramID                                  */
/*==============================================================*/
create index index_ChartDiagramID on tLogChartDiagram (
ChartDiagramID ASC
)
go

/*==============================================================*/
/* Table: tUser                                                 */
/*==============================================================*/
create table tUser (
   ID                   uniqueidentifier     not null,
   UserName             nvarchar(20)         not null,
   Password             varchar(50)          not null,
   IsAdmin              bit                  not null,
   DepartmentID         uniqueidentifier     not null,
   FullName             nvarchar(10)         null,
   Email                varchar(50)          null,
   Mobile               varchar(20)          null,
   AuthCode             varchar(32)          null,
   CreateTime           datetime             not null,
   IsStop               bit                  not null,
   Remark               nvarchar(400)        null,
   constraint PK_TUSER primary key (ID),
   constraint AK_UQ_USERNAME_TUSER unique (UserName)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '用户信息表',
   'user', @CurrentUser, 'table', 'tUser'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '标识',
   'user', @CurrentUser, 'table', 'tUser', 'column', 'ID'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '用户名',
   'user', @CurrentUser, 'table', 'tUser', 'column', 'UserName'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '密码',
   'user', @CurrentUser, 'table', 'tUser', 'column', 'Password'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '是否为管理员',
   'user', @CurrentUser, 'table', 'tUser', 'column', 'IsAdmin'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '所属部门',
   'user', @CurrentUser, 'table', 'tUser', 'column', 'DepartmentID'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '姓名',
   'user', @CurrentUser, 'table', 'tUser', 'column', 'FullName'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '邮箱',
   'user', @CurrentUser, 'table', 'tUser', 'column', 'Email'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '手机',
   'user', @CurrentUser, 'table', 'tUser', 'column', 'Mobile'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '授权码',
   'user', @CurrentUser, 'table', 'tUser', 'column', 'AuthCode'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '创建时间',
   'user', @CurrentUser, 'table', 'tUser', 'column', 'CreateTime'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '是否停用',
   'user', @CurrentUser, 'table', 'tUser', 'column', 'IsStop'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', @CurrentUser, 'table', 'tUser', 'column', 'Remark'
go

/*==============================================================*/
/* Index: index_UserName                                        */
/*==============================================================*/
create unique index index_UserName on tUser (
UserName ASC
)
go

/*==============================================================*/
/* Index: index_DepartmentID                                    */
/*==============================================================*/
create index index_DepartmentID on tUser (
DepartmentID ASC
)
go

/*==============================================================*/
/* Index: index_AuthCode                                        */
/*==============================================================*/
create unique index index_AuthCode on tUser (
AuthCode ASC
)
go

/*==============================================================*/
/* Table: tUserChart                                            */
/*==============================================================*/
create table tUserChart (
   ID                   bigint               identity,
   UserID               uniqueidentifier     not null,
   ChartID              uniqueidentifier     not null,
   Code                 tinyint              not null,
   constraint PK_TUSERCHART primary key (ID)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '用户图表授权表',
   'user', @CurrentUser, 'table', 'tUserChart'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '标识',
   'user', @CurrentUser, 'table', 'tUserChart', 'column', 'ID'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '用户标识',
   'user', @CurrentUser, 'table', 'tUserChart', 'column', 'UserID'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '图表标识',
   'user', @CurrentUser, 'table', 'tUserChart', 'column', 'ChartID'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '授权代码',
   'user', @CurrentUser, 'table', 'tUserChart', 'column', 'Code'
go
