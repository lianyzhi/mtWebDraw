﻿/// <reference path="../ExtJS/adapter/ext/ext-base.js" />
/// <reference path="../ExtJS/ext-all.js" />
/// <reference path="../ExtJS/ExtJSextend.js" />
/// <reference path="ExtJSCommon.js" />

//#region 公共变量与对象

var cookie = null; //Ext.util.Cookies对象
var user = null; //登陆用户信息

//#endregion

//#region 公共方法

//兼容Firefox的关闭当前页面方法
function ClosePage() {
    var ua = navigator.userAgent;
    if (ua.indexOf("MSIE") > 0) {
        window.opener = null; //防止浏览器提示脚本关闭页面
        window.open('about:blank', '_top');
        window.top.close();
    }
    else if (ua.indexOf("Firefox") != -1 || ua.indexOf("Chrome") != -1) {
        window.location.href = 'about:blank';
        window.close();
    }
    else {
        window.opener = null;
        window.open('about:blank', '_self');
        window.close();
    }
}

//关闭登陆窗口
function UserClose(p) {
    if (p) //p存在则为点击窗体X事件调用
    {
        loginForm = null;
        loginWin = null;

        if (user == null) { //未登陆时登陆窗体被关闭则关闭页面
            ClosePage();
        }
    }
    else {
        loginWin.close(); //释放窗体
    }
}

//#endregion

//#region 加载完脚本后执行

var loginForm = null; //登陆表单
var loginWin = null; //登陆窗体

Ext.onReady(function () {
    //使用Cookie
    cookie = Ext.util.Cookies;

    //获取登陆用户信息，如已登陆则获取到并赋值给user
    AjaxPost('/Ajax/LoginUserInfo', function (json) {
        if (json.code == 1) {
            user = json.data;
        }
    });

    //如已登陆则直接初始化界面
    if (user != null) {
        MainInit();
    }
    else //未登陆则初始化登陆界面
    {
        //启用表单提示
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'side';

        //登陆表单
        loginForm = new Ext.FormPanel({
            labelWidth: 60,
            labelAlign: 'right',
            border: false,
            defaultType: 'textfield',
            buttomAlign: 'center',
            bodyStyle: 'padding: 15px',
            items: [{
                fieldLabel: '用户名',
                name: 'loginName',
                id: 'loginName',
                anchor: '90%',
                allowBlank: false,
                blankText: '用户名不能为空',
                listeners: {
                    specialkey: function (field, e) {
                        if (e.getKey() == Ext.EventObject.ENTER) {
                            UserLogin();
                        }
                    }
                }
            }, {
                inputType: 'password',
                fieldLabel: '密码',
                name: 'loginPassword',
                id: 'loginPassword',
                anchor: '90%',
                allowBlank: false,
                blankText: '密码不能为空',
                listeners: {
                    specialkey: function (field, e) {
                        if (e.getKey() == Ext.EventObject.ENTER) {
                            UserLogin();
                        }
                    }
                }
            }, new Ext.form.Checkbox({
                id: 'loginCookie',
                checked: cookie.get('loginName') && cookie.get('loginPassword') ? true : false,
                fieldLabel: '记住密码',
                boxLabel: '请不要在网吧或者公共场所使用此功能！'
            })],
            buttons: [{
                text: '登陆',
                iconCls: 'icons-base-accept',
                handler: function () {
                    UserLogin();
                }
            }, {
                text: '退出',
                iconCls: 'icons-base-cancel',
                handler: function () {
                    UserClose();
                }
            }]
        });

        //登陆窗体
        loginWin = new Ext.Window({
            title: 'Web绘流系统用户登陆',
            layout: 'fit',
            width: 350,
            height: 170,
            closeAction: 'hide',
            iconCls: 'icons-login',
            plain: true,
            modal: true,
            maskDisabled: true,
            items: loginForm,
            listeners: {
                hide: function (p) {
                    UserClose(p);
                }
            }
        });

        //记住密码则自动填入密码
        if (Ext.getCmp('loginCookie').checked) {
            Ext.getCmp('loginName').value = cookie.get('loginName');
            Ext.getCmp('loginPassword').value = cookie.get('loginPassword');
        }

        //显示登陆窗体
        loginWin.show();

        //记住密码则自动登陆
        if (Ext.getCmp('loginCookie').checked) {
            UserLogin();
        }

    }

});

//#endregion

//#region 用户登陆及退出方法

//操作cookie
//type:true-设置cookie,false-清理cookie
function OperateCookie(type) {
    var nextYear = new Date();
    nextYear.setFullYear(nextYear.getFullYear() + 1);

    if (type) {
        cookie.set("loginName", Ext.getCmp("loginName").getValue(), nextYear);
        cookie.set("loginPassword", Ext.getCmp("loginPassword").getValue(), nextYear);
    }
    else {
        cookie.set("loginName", "", nextYear);
        cookie.set("loginPassword", "", nextYear);
    }
}

//用户登陆处理
function UserLogin() {
    FormSubmit(loginForm, '/Ajax/UserLogin', function (json) {
        if (json.code == 1) {
            OperateCookie(Ext.getCmp("loginCookie").checked);
            user = json.data;
            UserClose();
            MainInit();
        }
        else
            errorMessage(json.msg);
    });
}

//退出登陆及清除登陆信息
function UserExit(isClearCookie) {
    if (isClearCookie) //清理cookie
    {
        OperateCookie(false);
    }

    //退出登陆
    AjaxPost('/Ajax/UserExit', function (json) {
        if (json.code == 1) {
            alertMessage("退出成功！");
        }
    });

    //关闭页面
    ClosePage();
}

//返回用户信息
function UserInfo() {
    return user;
}

//#endregion



//------------------------------------ 向上为Login ------------------------------------

//#region 承上启下的界面加载方法

function MainInit() {
    CreateView();
}

//#endregion

//----------------------------------- 向下为Viewport ----------------------------------



//#region 公共变量

var view = null; //布局容器
var toolbar = null; //布局容器工具栏
var view_tabPanel = null; //布局容器显示内容的TabPanel
var view_status = null; //布局容器底部状态栏中最后一栏状态

//#endregion

//#region 创建工具栏

//创建布局容器工具栏
function CreateToolbar() {

    //#region Menu_YongHu

    var YongHu1 = new Ext.menu.Item({
        text: '部门管理',
        iconCls: 'icons-department',
        handler: function () {
            toolbarMenuItemClick("YongHu1", this.text, this.iconCls);
        }
    });

    var YongHu2 = new Ext.menu.Item({
        text: '用户管理',
        iconCls: 'icons-user',
        handler: function () {
            toolbarMenuItemClick("YongHu2", this.text, this.iconCls);
        }
    });

    var Menu_YongHu = new Ext.menu.Menu({
        shadow: 'sides',
        items: [YongHu1, YongHu2]
    });

    //#endregion

    //#region Menu_LiuCheng

    var LiuCheng1 = new Ext.menu.Item({
        text: '流程图管理',
        iconCls: 'icons-chart',
        handler: function () {
            toolbarMenuItemClick("LiuCheng1", this.text, this.iconCls);
        }
    });

    var Menu_LiuCheng = new Ext.menu.Menu({
        shadow: 'sides',
        items: [LiuCheng1]
    });

    //#endregion

    //#region Menu_XiTong

    var XiTong1 = new Ext.menu.Item({
        text: '系统介绍',
        iconCls: 'icons-system-caption',
        handler: function () {
            toolbarMenuItemClick("XiTong1", this.text, this.iconCls);
        }
    });

    var XiTong2 = new Ext.menu.Item({
        text: '系统使用说明',
        iconCls: 'icons-system-readme',
        handler: function () {
            toolbarMenuItemClick("XiTong2", this.text, this.iconCls);
        }
    });

    var Menu_XiTong = new Ext.menu.Menu({
        shadow: 'sides',
        items: [XiTong1, XiTong2]
    });

    //#endregion

    //#region Menu_Exit

    var Exit1 = new Ext.menu.Item({
        text: '退出登陆',
        iconCls: 'icons-logout',
        handler: function () {
            UserExit(false);
        }
    });
    var Exit2 = new Ext.menu.Item({
        text: '清除登陆',
        iconCls: 'icons-logout-clear',
        handler: function () {
            UserExit(true);
        }
    });
    var Menu_Exit = new Ext.menu.Menu({
        shadow: 'sides',
        items: [Exit1, Exit2]
    });

    //#endregion

    //#region toolbar

    toolbar = new Ext.Toolbar({
        autoWidth: true
    });
    toolbar.add({
        text: '用户管理',
        id: 'Menu_YongHu',
        iconCls: 'icons-user',
        menu: Menu_YongHu
    });
    toolbar.add({
        text: '流程图管理',
        id: 'Menu_LiuCheng',
        iconCls: 'icons-chart',
        menu: Menu_LiuCheng
    });
    toolbar.add({
        text: '系统信息',
        id: 'Menu_XiTong',
        iconCls: 'icons-system-info',
        menu: Menu_XiTong
    });
    toolbar.add('->');
    toolbar.add({
        text: '退出',
        iconCls: 'icons-logout',
        id: 'Menu_Exit',
        menu: Menu_Exit
    });

    //#endregion

}

//#endregion

//#region 创建并初始化布局

function CreateView() {
    CreateToolbar();

    //创建布局容器显示内容的TabPanel
    view_tabPanel = new Ext.TabPanel({
        region: 'center',
        id: 'view_tabPanel',
        activeTab: 0,
        border: false,
        items: []
    });

    var fuwuqi = new Ext.Toolbar.TextItem('<span class=\"icons-size icons-server\"></span><span class=\"icons-font\">服务器版本：V1.0.0.0</span>');
    var yonghu = new Ext.Toolbar.TextItem("<span class=\"icons-size icons-user\"></span><span class=\"icons-font\">用户：" + (user.FullName ? user.FullName : user.UserName) + "</span>");
    var clock = new Ext.Toolbar.TextItem('');
    view_status = new Ext.Toolbar.TextItem('');

    view = new Ext.Viewport({
        layout: 'fit',
        items: [
            new Ext.Panel({
                region: 'center',
                layout: 'border',
                bbar: {
                    id: 'stabar',
                    items: [fuwuqi, '-', yonghu, '->', clock, '-', view_status]
                },
                listeners: {
                    'render': {
                        fn: function () {
                            try {
                                Ext.TaskMgr.start({
                                    run: function () {
                                        Ext.fly(clock.getEl()).update('<span class=\"icons-size icons-time\"></span><span class=\"icons-font\">本地时间：' + (new Date().format('A g:i:s')) + "</span>");
                                    },
                                    interval: 1000
                                });
                            } catch (e) { }
                        }
                    }
                },
                tbar: toolbar,
                items: [
                    view_tabPanel
                ]
            })
        ]
    });

    Ajax_wancheng();

    if (LoadInfo.chartid == "") { //若指定图形则打开图形界面
        //Tab加载系统使用说明
        toolbarMenuItemClick("XiTong2", "系统使用说明", 'icons-system-readme');
    } else {
        toolbarMenuItemClick("LiuCheng1", '流程图管理', 'icons-chart');
    }
}

//#endregion

//#region 界面底部状态栏状态

//请求数据连接
function Ajax_lianjie() {
    Ext.fly(view_status.getEl()).update("<span class=\"icons-font font-red\">正在连接服务器。。。</span><span class=\"icons-size icons-server-connect\"></span>");
}

//请求数据完成
function Ajax_wancheng() {
    Ext.fly(view_status.getEl()).update("<span class=\"icons-size icons-server-connected\"></span>");
}

//请求数据出错
function Ajax_chucuo() {
    Ext.fly(view_status.getEl()).update("<span class=\"icons-font font-red\">连接服务器出错。。。</span><span class=\"icons-size icons-server-error\"></span>");
}

//设置底部工具栏状态消息
function SetbbarStatus(message) {
    Ext.fly(view_status.getEl()).update("<span class=\"icons-font font-red\">" + message + "</span><span class=\"icons-size icons-server-connected\"></span>");

    setTimeout(Ajax_wancheng, 3000);
}

//#endregion

//#region 对外方法：工具栏点击事件及创建TabPanel显示项并显示

//工具栏菜单项点击事件
function toolbarMenuItemClick(menuItemName, menuItemTitle, menuIconCls)
{ 
    if (menuItemName == "YongHu1")
        CreateTabShow(menuItemName, menuItemTitle, menuIconCls, "/Home?v=Department");
    else if (menuItemName == "YongHu2")
        CreateTabShow(menuItemName, menuItemTitle, menuIconCls, "/Home?v=User");
    else if (menuItemName == "LiuCheng1")
        CreateTabShow(menuItemName, menuItemTitle, menuIconCls, "/Home?v=Chart");
    else if (menuItemName == "XiTong1")
        CreateTabShow(menuItemName, menuItemTitle, menuIconCls, "/About?v=Remark");
    else if (menuItemName == "XiTong2")
        CreateTabShow(menuItemName, menuItemTitle, menuIconCls, "/About?v=ReadMe");
    else
        errorMessage("菜单项“" + menuItemTitle + "”未找到其关联功能页面！");
}

//创建TabPanel显示项并显示
//tabId:tab+项名称,如tabXiTong2；tabTitle:选项卡标题；tabUrl:选项卡显示页面地址
function CreateTabShow(tabId, tabTitle, tabIconCls, tabUrl)
{
    view_tabPanel.add({
        id: "tab" + tabId,
        title: tabTitle,
        iconCls: tabIconCls ? tabIconCls : null,
        closable: true,
        layout: 'fit',
        items: [new Ext.ux.IFrameComponent({ id: 'iframe_' + tabId, url: tabUrl })]
    });
    //显示当前新增的tab
    view_tabPanel.setActiveTab("tab" + tabId);
}

//#endregion
