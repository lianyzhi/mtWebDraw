﻿/// <reference path="../ExtJS/adapter/ext/ext-base.js" />
/// <reference path="../ExtJS/ext-all.js" />
/// <reference path="../ExtJS/ExtJSextend.js" />

//ExtJS封装的扩展方法

//弹出提示框
function alertMessage(msg) {
    Ext.MessageBox.show({
        title: '提示',
        msg: msg,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.INFO
    });
}

//弹出错误提示框
function errorMessage(error) {
    Ext.MessageBox.show({
        title: '提示',
        msg: error,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.ERROR
    });
}

//Ext.Ajax.request以同步方式进行post请求数据：url-请求地址,successFunc-成功后回调方法
//示例：AjaxPost('/Ajax/LoginUserInfo', function (json) {
//    if (json.code == 1) { user = json.data; } else { errorMessage(json.msg); }
//});
function AjaxPost(url, successFunc) {
    Ext.Ajax.request({
        url: url,
        method: 'post',
        async: false, //是否异步(true异步, false同步)
        params: '',
        success: function (response, opts) {
            var json = eval("(" + response.responseText + ")");
            successFunc(json);
        }
    });
}

//表单提交通用方法：varForm-是Ext.FormPanel的变量名,url-表单提交地址,successFunc-成功后回调方法
//示例：FormSubmit(loginForm, '/Ajax/UserLogin', function (json) {
//    if (json.code == 1) { user = json.data; } else errorMessage(json.msg);
//});
function FormSubmit(varForm, url, successFunc) {
    if (varForm.form.isValid()) {
        varForm.form.doAction('submit', {
            url: url,
            method: 'post',
            params: '',
            clientValidation: true,
            submitEmptyText: false,
            waitMsg: '正在登陆中......',
            waitTitle: '提示',
            success: function (form, action) {
                successFunc(action.result);
            },
            failure: function (form, action) {
                var error = "";
                if (action.failureType == 'connect') {
                    error = '网络超时，请稍候再试！';
                } else {
                    error = "未知网络错误！";
                }
                errorMessage(error);
            }
        });
    }
}

//默认获取表格选中行数据，如rowIndex存在或为0（即右键菜单）则取当前右键的列
//示例：var row = GetSelectionRow('grid', rowIndex);
function GetSelectionRow(gridId, rowIndex)
{
    var row = null;

    if (rowIndex || rowIndex == 0) {
        row = Ext.getCmp(gridId).getStore().getAt(rowIndex);
    }
    else {
        var rows = Ext.getCmp(gridId).getSelectionModel().getSelections();
        if (rows && rows.length > 0) {
            row = rows[0];
        }
    }

    return row;
}

//设置Bool类型列显示为是或否
function SetGridBoolValue(value, metaData, record, rowIndex, colIndex, store) {
    if (value == true || value == 1) {
        value = "是";
    } else {
        value = "否";
    }
    return value;
}

//获取行扩展时使用的前置空白字符
function GetRowExpanderNbsp(isNoRowNumber) {
    if (isNoRowNumber)
        return '　&nbsp;　&nbsp;　';
    else
        return '　&nbsp;　&nbsp;　&nbsp;　&nbsp;　';
}
